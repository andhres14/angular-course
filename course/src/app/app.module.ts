import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import { routing, appRoutingProviders} from "./app.routing";

import { AppComponent } from './app.component';
import {FruitComponent} from "./fruit/fruit.component";
import {EmployeeComponet} from "./employee/employee.componet";
import {HomeComponent} from "./home/home.component";
import {ContactComponent} from "./contact/contact.component";

@NgModule({
  declarations: [
    AppComponent,
    FruitComponent,
    EmployeeComponet,
    HomeComponent,
    ContactComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    routing
  ],
  providers: [appRoutingProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
