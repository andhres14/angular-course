import {Component} from "@angular/core";
import {Employee} from "./employee";
import {s} from "@angular/core/src/render3";

@Component({
  selector: 'employee',
  templateUrl: './employee.component.html'
})

export class EmployeeComponet {
  public name_employee = "Componente Empleados";
  public employee:Employee;
  public workers:Array<Employee>;
  public extern_worker = true;
  public colour:string;
  public colour_selected:string;

  constructor(){
    this.employee = new Employee('Andres Buitrago', 20, 'Desarrollador web', true);
    this.workers = [
      new Employee('Andres Quitian', 20, 'Desarrollador web', true),
      new Employee('Laura Quitian', 18, 'Desarrollador web', true),
      new Employee('Adriana Quitian', 39, 'Desarrollador web', true)
    ];
    this.colour = 'blue';
    this.colour_selected = 'RED';
  }
  ngOnInit(){
    console.log(this.workers)
  }

  changeExtern(value){
    this.extern_worker = value;
  }

}
