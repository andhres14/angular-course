import {Component} from "@angular/core";

@Component({
  selector: 'fruit',
  templateUrl: './fruit.component.html'
})

export class FruitComponent {
  public name_fruit:string;
  public fruit_list:string;
  public name:string;
  public age:number;
  public adult:boolean;
  public works:Array<string>;
  public comodin:any;

  constructor(){
    this.name_fruit = "Componente de fruta";
    this.fruit_list = "Naranja, Manazana, Pera";
    this.name = "Andres Buitrago";
    this.age = 20;
    this.adult = true;
    this.works = ['Carpintero', 'Programador Web'];
    this.comodin = 232;
  }

  ngOnInit(){
    this.holaMundo(this.name, this.age);

    //variable
    var one = 9;
    var two = 15;
    let three = 2;

    if (one === 4){
      let one = 5;
      console.log("IN IF "+one);
    }else{
      console.log("OUT IF "+4);
    }

    console.log(one);

  }

  holaMundo(name:string,  age:number){
    console.log(name+' - '+age)
  }
}
